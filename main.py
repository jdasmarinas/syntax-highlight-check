def test_func():
    """This is a test function
    """
    print("Hello world!")

def test_func2():
    r"""Another test function

    Test here docs

    >>> test(' ')
    4
    """
    print("Hello world")

# Taken from https://gitlab.com/asottile/flake8/-/blob/eb6228b660a5194f201dcf8767f804f697756b79/src/flake8/processor.py
def expand_indent(line):
    r"""Return the amount of indentation.

    Tabs are expanded to the next multiple of 8.

    >>> expand_indent('    ')
    4
    >>> expand_indent('\t')
    8
    >>> expand_indent('       \t')
    8
    >>> expand_indent('        \t')
    16
    """
    if '\t' not in line:
        return len(line) - len(line.lstrip())
    result = 0
    for char in line:
        if char == '\t':
            result = result // 8 * 8 + 8
        elif char == ' ':
            result += 1
        else:
            break
    return result


